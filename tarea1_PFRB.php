<?php
// Create a centered pyramid of stars
$n = 30;
$star = "*";
$breakline = "<br/>";
$line = "";
echo "<h1>Piramide</h1>";
for ($i=1; $i <= $n; $i++) { 
  echo "<p style='height:1px; padding:0 0; font-size: 20px; margin: 0 0; text-align:center'>";
  for ($j=1; $j <= $i; $j++) { 
    echo $star;
  } 
  echo "</p>";
  echo $breakline; 
}
echo "<h1>Rombo</h1>";
for ($i=1; $i <= $n/2; $i++) { 
  echo "<p style='height:1px; padding:0 0; font-size: 20px; margin: 0 0; text-align:center'>";
  for ($j=1; $j <= $i; $j++) { 
    echo $star;
  } 
  echo "</p>";
  echo $breakline; 
}
$n = 30;
for ($i=$n/2; $i >= 1; $i--) { 
  echo "<p style='height:1px; padding:0 0; font-size: 20px; margin: 0 0; text-align:center'>";
  for ($j=1; $j <= $i; $j++) { 
    echo $star;
  } 
  echo "</p>";
  echo $breakline; 
}