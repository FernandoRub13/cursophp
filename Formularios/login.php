<?php 
  session_start();
  if(isset($_COOKIE['Alumno_actual'])) {
    header('Location: info.php');
  }
  if(!isset($_SESSION['Alumno'])){
    $_SESSION['Alumno'] = [];
    // push a new user into array 
  array_push($_SESSION['Alumno'], [
    'num_cuenta' => '1',
    'nombre' => 'Admin',
    'primer_apellido' => 'General',
    'segundo_apellido' => '',
    'contrasena' => 'adminpass123' ,
    'genero' => 'O' ,
    'fecha_nac' => '25/01/1990', 
  ]);
  }
  

  if (!empty($_POST)) {
    foreach($_SESSION['Alumno'] as $alumno) {
      if ($alumno['contrasena'] == $_POST['contrasena'] && $alumno['num_cuenta'] == $_POST['num_cuenta']) {
        setcookie('Alumno_actual', serialize($alumno), "0");
        header('Location: info.php');
        exit;
      }else{
        $error = 'Usuario o contraseña incorrectos. Por favor, intentalo nuevamente.';
      }
    }
    
  }
  


?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Login</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">


</head>
<body>
  <div class="container">
  <!-- Make a repsonsive login -->
    <div class="row">
      <div class="col-sm-8 offset-sm-2 col-md-6 offset-md-3">
        <div class="card "style="margin: 50% auto;">
          <div class="card-header">
            <h1 style="text-align: center;" >Login</h1>
          </div>
        <div class="card-body">
        <form  method="post">
          <!-- Alert  -->
          <?php if(isset($error)) : ?>
            <div class="alert alert-danger" role="alert">
              <?php echo $error; ?>
            </div>
          <?php endif; ?>

          <div class="form-group">
            <label for="num_cuenta">Numero de Cuenta: </label>
            <input type="num_cuenta" name="num_cuenta" id="num_cuenta" class="form-control">
          </div>
          <div class="form-group">
            <label for="contrasena">Contraseña: </label>
            <input type="password" name="contrasena" id="contrasena" class="form-control">
          </div>
          <button type="submit" class="mt-4 btn btn-primary">Login</button>
        </form>
          </div>
        </div>
      </div>
    </div>
  </div>
    </div>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
</body>
</html>