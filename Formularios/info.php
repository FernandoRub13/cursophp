<?php
// inicia la sesion
session_start();
// determina su hay un usuario loggeado
if (!isset($_COOKIE['Alumno_actual'])) {
    header('Location: login.php');
} else {
    $array = unserialize($_COOKIE['Alumno_actual']);
}
// Function that deletes the cookie
function deleteCookie()
{
    setcookie('Alumno_actual', '', time() - 3600);
}
//  determinates if the user looged out and deletes the cookie
if (!empty($_POST)) {
    deleteCookie();
    header('Location: login.php');

} 
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Info</title>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
</head>
<body>
  <!-- Make a responsive navbar -->
  
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        
        <li class="nav-item">
          <a class="nav-link" href="formulario.php">Registar alumnos </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="info.php">Información </a>
        </li>
        <li >
          <form  method="post">
            <input type="hidden" name="cerrar-sesion">
            <input type="submit" class="nav-item"  name="logout" value="Cerrar sesión" >
          </form>
        </li>
      </ul>
    </div>
  </nav>

  <div class="row">
      <div class="col-sm-8 offset-sm-2 col-md-6 offset-md-3">
        <h2 style="margin: 20% auto 10px auto" >Usuario autenticado</h2>
        <div class="card "style="margin: 0 auto 0 auto;">
          <div class="card-header">
            <h3 style="text-align: center;" ><?php echo $array['nombre'] .
                ' ' .
                $array['primer_apellido']; ?></h3>
          </div>
          <div class="card-body">
            <h2 class="card-title">Información</h2>
            <h5>Número de cuenta: <?php echo $array['num_cuenta']  ?></h5>
            <h5>Fecha de nacimiento : <?php echo $array['fecha_nac']  ?></h5>
          </div>
        </div>
        <h2 style="margin: 20% auto 10px auto" >Datos guardados: </h2>
        <!-- In a table list all the users stored in $_SESSION -->
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nombre</th>
              <th scope="col">Fecha de nacimiento</th>
            </tr>
          </thead>
          <tbody>
            <?php
            foreach ($_SESSION['Alumno'] as $alumno) {
                echo '<tr>';
                echo '<td>' . $alumno['num_cuenta'] . '</td>';
                echo '<td>' . $alumno['nombre'] . " " . $alumno['primer_apellido'] . " " . $alumno['segundo_apellido'] . '</td>';
                echo '<td>' . $alumno['fecha_nac'] . '</td>';
                echo '</tr>';
            }
            ?>
          </tbody>

      </div>
    </div>
  

  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script></body>
</html>