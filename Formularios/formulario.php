<?php
session_start();
if (!isset($_COOKIE['Alumno_actual'])) {
    header('Location: login.php');
}
// Function that deletes the cookie
function deleteCookie()
{
    setcookie('Alumno_actual', '', time() - 3600);
}

if (!empty($_POST)) {
    if (isset($_POST['cerrar-sesion'])) {
        deleteCookie();
        header('Location: login.php');
        exit;
    }
  array_push($_SESSION['Alumno'], [
    'num_cuenta' => $_POST['num_cuenta'],
    'nombre' => $_POST['nombre'],
    'primer_apellido' => $_POST['primer_apellido'],
    'segundo_apellido' => $_POST['segundo_apellido'],
    'contrasena' => $_POST['contrasena'],
    'genero' => $_POST['genero'],
    'fecha_nac' => date("d/m/Y", strtotime($_POST['fecha_nac']))  
  ]);
  $success = "Usuario creado con éxito";
  // header('Location: formulario.php');
}else{
  $error = "Error al crear usuario";
  
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        
        <li class="nav-item">
          <a class="nav-link" href="formulario.php">Registar alumnos </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="info.php">Información </a>
        </li>
        <li >
          <form  method="post">
          <input type="hidden" name="cerrar-sesion">
            <input type="submit" class="nav-item"  name="logout" value="Cerrar sesión" >
          </form>
        </li>
      </ul>
    </div>
  </nav>

  
  <div class="row">
      <div class="col-sm-8 offset-sm-2 col-md-6 offset-md-3">
        <h2 style="margin: 20% auto 10px auto" >Registrar un alumno</h2>
        <div class="card "style="margin: 0 auto 0 auto;">
          
          <div class="card-body">
            <!-- Make a form to create an Alumno -->
            <?php if(isset($success)) : ?>
            <div class="alert alert-success" role="alert">
              <?php echo $success; ?>
            </div>
          <?php endif; ?>
            <form method="post" action="formulario.php">
              <div class="form-group">
                <label for="num_cuenta">Número de cuenta: </label>
                <input required type="number" class="form-control" id="num_cuenta" name="num_cuenta" placeholder="Número de cuenta">
              </div>
              <div class="form-group">
                <label for="nombre">Nombre</label>
                <input required type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">
              </div>
              <div class="form-group">
                <label for="primer_apellido">Primer Apellido</label>
                <input required type="text" class="form-control" id="primer_apellido" name="primer_apellido" placeholder="Primer Apellido">
              </div>
              <div class="form-group">
                <label for="segundo_apellido">Segundo Apellido</label>
                <input  type="text" class="form-control" id="segundo_apellido" name="segundo_apellido" placeholder="Segundo Apellido">
              </div>
              <!-- form radio control-->
              <div class="form-group">
                <label for="genero">Genero</label>
                <div class="form-check">
                  <input required class="form-check-input required" type="radio" name="genero" id="generoH" value="H">
                  <label class="form-check-label" for="generoH">
                    Hombre
                  </label>
                </div>
                <div class="form-check">
                  <input required class="form-check-input required" type="radio" name="genero" id="generoM" value="M">
                  <label class="form-check-label" for="generoM">
                    Mujer
                  </label>
                </div>
                <div class="form-check">
                  <input required class="form-check-input required" type="radio" name="genero" id="generoO" value="O">
                  <label class="form-check-label" for="generoO">
                    Otro
                  </label>
                </div>
              </div>
              <div class="form-group">
                <label for="fecha_nac">Fecha de nacimiento</label>
                <input required type="date" class="form-control" id="fecha_nac" name="fecha_nac" placeholder="Fecha de nacimiento">
              </div>
              <div class="form-group">
                <label for="contrasena">Password</label>
                <input required type="password" class="form-control" id="contrasena" name="contrasena" placeholder="Password">
              </div>
              <button type="submit" class="btn btn-primary" name="submit">Submit</button>
            </form>
        </div>
        

      </div>
  </div>
  
</body>
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script></body>
</html>