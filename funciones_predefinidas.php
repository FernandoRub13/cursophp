  substr // Devuelve una parte de una cadena
  strstr // Encuentra la primera ocurrencia de una cadena en otra cadena y devuelve la parte de la cadena que se encuentra despues de la primera ocurrencia
strpos // Busca la primera ocurrencia de una cadena en otra cadena y devuelve la posicion de la primera ocurrencia
  implode // Convierte un array en una cadena
  explode // Convierte una cadena en un array
  utf8_encode // Convierte una cadena a UTF-8
  utf8_decode // Convierte una cadena con caracteres ISO-8859-1 codificados con UTF-8 a ISO-8859-1 de un solo byte ‎
  array_pop // Elimina el último elemento de un array
  array_push // Añade un elemento al final de un array
  array_diff // Devuelve un array con los elementos que no están en el otro array
  array_walk // Ejecuta una función sobre cada uno de los elementos de un array
  sort // Ordena un array
  current // Devuelve el elemento actual de un array
  date // Devuelve la fecha actual
  empty // Comprueba si una variable está vacía
  isset // Comprueba si una variable está definida y si es diferente a nullo
serialize // Convierte una variable en una cadena
unserialize // Convierte una cadena en una variable