<?php 
//Realizar una expresión regular que detecte emails correctos.
$checkEmail="pablo@gmail.com";
$regex = "/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+$/";
if (preg_match($regex, $checkEmail)) {
    echo "El email es correcto";
} else {
    echo "El email es incorrecto";
}

echo "<br>";
echo "<hr/>";
//Realizar una expresion regular que detecte Curps Correctos
//ABCD123456EFGHIJ78.
$checkCurp="RUBP010313HDFBLBA1";
$regex = "/^[a-z0-9]{18}$/i";
if (preg_match($regex, $checkCurp)) {
    echo "La curp es correcta";
} else {
    echo "La curp es incorrecta";
}


echo "<br>";
echo "<hr/>";
//Realizar una expresion regular que detecte palabras de longitud mayor a 50
//formadas solo por letras.
$checkPalabra="Ferrocarril";
$regex = "/^[a-zA-Z]{51,}$/";
if (preg_match($regex, $checkPalabra)) {
    echo "La palabra tiene más de 50 letras";
} else {
    echo "La palabra no tiene más de 50 letras";
}


echo "<br>";
echo "<hr/>";
//Crea una funcion para escapar los simbolos especiales de una cadena.
$texto = "Hola niño, ¿cómo estás? Estoy bien, gracias.";
$textoEscapado = preg_quote($texto, "/");
echo $textoEscapado;


echo "<br>";
echo "<hr/>";
//Crear una expresion regular para detectar números decimales.
$checkNumero="12";
$regex = "/^[0-9]{1,}.[0-9]{1,}$/";
if (preg_match($regex, $checkNumero)) {
    echo "El número es decimal";
} else {
    echo "El número es no es decimal";
}